﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Lab2.EF
{
    public abstract class UserBase
    {
        [JsonProperty(propertyName: "name")]
        public string Name { get; set; }

        [JsonProperty(propertyName: "surname")]
        public string Surname { get; set; }

        [JsonProperty(propertyName: "email")]
        public string Email { get; set; }
    }
}
