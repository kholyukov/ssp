﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Lab2.EF
{
    public class User : UserBase
    {
        [Key]
        public int Id { get; set; }

        [Column("Password")]
        public byte[] PasswordHash { get; set; }

        [Column("Salt")]
        public byte[] PasswordSalt { get; set; }
    }
}
