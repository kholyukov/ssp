﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Lab2.EF
{
    public class Book
    {
        [Key]
        [Column(TypeName ="int(11)")]
        [JsonProperty(propertyName: "id")]
        public string Id { get; set; }

        [MaxLength(100)]
        [JsonProperty(propertyName:"name")]
        public string Name { get; set; }

        [JsonProperty(propertyName: "author")]
        public string Author { get; set; }

        [JsonProperty(propertyName: "price")]
        public double Price { get; set; }

        [MaxLength(300)]
        [JsonProperty(propertyName: "description")]
        public string Description { get; set; }

        [MaxLength(70)]
        [JsonProperty(propertyName: "imageLocation")]
        public string ImageLocation { get; set; }

        [JsonProperty(propertyName: "releaseDate")]
        public DateTime? ReleaseDate { get; set; }
    }
}
