﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Lab2.EF;
using Lab2.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Lab2.Controllers
{
    public class XmlController : Controller
    {
        IHostingEnvironment _config;
        private IBookService _bookService;

        public XmlController(IHostingEnvironment configuration, IBookService bookService)
        {
            _config = configuration;
            _bookService = bookService;
        }

        public IActionResult Download()
        {
            XElement root = new XElement("Books");
            XDocument document = new XDocument(root);
            var bookList = _bookService.GetAll();
            foreach (var book in bookList)
            {
                XElement xbook = new XElement("Book",
                    new XElement("Id", book.Id),
                    new XElement("Name", book.Name),
                    new XElement("Author", book.Author),
                    new XElement("Price", book.Price),
                    new XElement("Description", book.Description),
                    new XElement("ReleaseDate", book.ReleaseDate),
                    new XElement("Image", book.ImageLocation));
                root.Add(xbook);
            }
            var filePath = Path.Combine(_config.WebRootPath, "files", "products.xml");
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                document.Save(stream);
            }
            return File("/files/products.xml", "text/xml", "products.xml");
        }

        public IActionResult Upload(IFormFile userFile)
        {
            XDocument xdocument;
            var books = _bookService.GetAll();
            if (userFile!=null && userFile.FileName.EndsWith(".xml"))
            {
                using (var stream = userFile.OpenReadStream())
                {
                    xdocument = XDocument.Load(stream);
                }
                var rootEl = xdocument.Root;
                if (rootEl.Name == "Books")
                {
                    var xBooks = from b in rootEl.Descendants("Book")
                                 select new Book
                                 {
                                     Id = b.Element("Id").Value,
                                     Name = b.Element("Name").Value,
                                     Author = b.Element("Author").Value,
                                     Description = b.Element("Description").Value,
                                     Price = int.Parse(b.Element("Price").Value),
                                     ReleaseDate = string.IsNullOrEmpty(b.Element("ReleaseDate").Value) ? (DateTime?)null : XmlConvert.ToDateTime(b.Element("ReleaseDate").Value, "yyyy-MM-ddTHH:mm:ss"),
                                     ImageLocation = b.Element("Image").Value
                                 };
                    bool bookExist;
                    foreach(var xBook in xBooks)
                    {
                        bookExist = false;
                        foreach(var book in books)
                        {
                            if (xBook.Name == book.Name && xBook.Author == book.Author)
                            {
                                bookExist = true;
                                break;
                            }
                        }
                        if (!bookExist)
                        {
                            _bookService.Create(xBook);
                        }
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }
    }
}