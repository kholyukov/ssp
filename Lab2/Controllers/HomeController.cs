﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lab2.Models;
using Lab2.EF;
using Lab2.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Lab2.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        IBookService _bookService;

        public HomeController(IBookService service)
        {
            _bookService = service;
        }

        public IActionResult Index()
        {
            return View(_bookService.GetAll());
        }

        [HttpPost]
        public IActionResult Create(CreateVM model)
        { 
            if(ModelState.IsValid)
            {
                _bookService.Create(model);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult GetInfo(string id)
        {
            return Json(_bookService.Get(id));
        }

        [HttpPost]
        public IActionResult Edit(EditVM model)
        {
            if (ModelState.IsValid)
            {
                _bookService.Edit(model);
            }
            return RedirectToAction("Index");
        }

        public IActionResult Delete(string id)
        {
            _bookService.Delete(id);
            return RedirectToAction("Index");
        }

        public IActionResult Search(string bookname)
        {
            var model = _bookService.GetByName(bookname);
            return View("Index", model);
        }
    }
}
