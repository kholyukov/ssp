﻿using Lab2.Cosmos;
using Lab2.EF;
using Lab2.Models;
using Lab2.Services;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.CosmosServices
{
    public class CosmosUserService : IUserService
    {

        public async Task CreateAsync(RegisterViewModel model)
        {
            string passwordSalt, passwordHash;
            CreatePasswordHash(model.PasswordString, out passwordSalt, out passwordHash);
            await CosmosContext.Client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri("ssp", "users"), new CosmosUser
            {
                Email = model.Email,
                Name = model.Name,
                Surname = model.Surname,
                PasswordSalt = passwordSalt,
                PasswordHash = passwordHash
            });
        }

        public async Task<UserBase> FindOnLoginAsync(string email, string passwordString)
        {
            var users = CosmosContext.Client.CreateDocumentQuery<CosmosUser>(UriFactory.CreateDocumentCollectionUri("ssp", "users")).Where(u => u.Email == email);
            var user = users.AsEnumerable().SingleOrDefault();
            if (user != null)
            {
                bool wrongPassword = !VerifyPasswordHash(passwordString, user.PasswordHash, user.PasswordSalt);
                if (wrongPassword)
                    return null;
            }
            return user;
        }


        public async Task<bool> IsEmailExistAsync(string email)
        {
            IOrderedQueryable<CosmosUser> users = CosmosContext.Client.CreateDocumentQuery<CosmosUser>(UriFactory.CreateDocumentCollectionUri("ssp", "users"));
            bool emailExist = users.AsEnumerable().Any(u => u.Email == email);

            return emailExist;
        }

        private void CreatePasswordHash(string passwordString, out string passwordSalt, out string passwordHash)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = Convert.ToBase64String(hmac.Key);
                passwordHash = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(passwordString)));
            }
        }

        private bool VerifyPasswordHash(string passwordString, string storedHash, string storedSalt)
        {
            byte[] salt, hash;
            salt = Convert.FromBase64String(storedSalt);
            hash = Convert.FromBase64String(storedHash);

            using (var hmac = new HMACSHA512(salt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(passwordString));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != hash[i])
                        return false;
                }
                return true;
            }
        }
    }
}
