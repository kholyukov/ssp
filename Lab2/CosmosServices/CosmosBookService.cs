﻿using Lab2.Cosmos;
using Lab2.EF;
using Lab2.Models;
using Lab2.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Lab2.CosmosServices
{
    public class CosmosBookService : IBookService
    {
        IHostingEnvironment _environment;

        public CosmosBookService(IHostingEnvironment env)
        {
            _environment = env;
        }

        public void Create(CreateVM model)
        {
            Book book = new Book
            {
                Author = model.Author,
                Description = model.Description,
                Name = model.Name,
                Price = model.Price,
                ReleaseDate = model.ReleaseDate
            };
            if (model.Image != null)
            {
                string path = @"\images\" + $"{model.Image.FileName}-{model.Image.FileName}";
                string absolutePath = _environment.WebRootPath + path;
                using (var fileStream = new FileStream(absolutePath, FileMode.Create))
                {
                    model.Image.CopyTo(fileStream);
                }
                book.ImageLocation = path;
            }

            this.Create(book);
        }

        public void Create(Book book)
        {
            CosmosContext.Client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri("ssp", "books"), book);
        }

        public void Delete(string id)
        {
            CosmosContext.Client.DeleteDocumentAsync(UriFactory.CreateDocumentUri("ssp", "books", id));
        }

        public void Edit(EditVM model)
        {
            var storedBook = CosmosContext.Client.ReadDocumentAsync<Book>(UriFactory.CreateDocumentUri("ssp", "books", model.Id)).GetAwaiter().GetResult();

            Book book = new Book()
            {
                Name = model.Name,
                Price = model.Price,
                Id = storedBook.Document.Id
            };
            book.ReleaseDate = model.ReleaseDate ?? storedBook.Document.ReleaseDate;
            book.Author = model.Author;
            book.Description = model.Description;
            if (model.Image != null)
            {
                if (storedBook.Document.ImageLocation != null)
                    File.Delete(_environment.WebRootPath + storedBook.Document.ImageLocation);
                string path = @"\images\" + $"{model.Image.FileName}-{model.Image.FileName}";
                string absolutePath = _environment.WebRootPath + path;
                using (var fileStream = new FileStream(absolutePath, FileMode.Create))
                {
                    model.Image.CopyTo(fileStream);
                }
                book.ImageLocation = path;
            }
            else
            {
                book.ImageLocation = storedBook.Document.ImageLocation;
            }

            CosmosContext.Client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri("ssp", "books", model.Id), book);
        }

        public Book Get(string id)
        {
            var books = CosmosContext.Client.CreateDocumentQuery<Book>(UriFactory.CreateDocumentCollectionUri("ssp", "books")).Where(b => b.Id == id);
            return books.AsEnumerable().SingleOrDefault();
        }

        public IEnumerable<Book> GetByName(string bookName)
        {
            var books = CosmosContext.Client.CreateDocumentQuery<Book>(UriFactory.CreateDocumentCollectionUri("ssp", "books")).Where(b => b.Name.Contains(bookName, StringComparison.OrdinalIgnoreCase));
            return books.AsEnumerable();
        }

        public IEnumerable<Book> GetAll()
        {
            var books = CosmosContext.Client.CreateDocumentQuery<Book>(UriFactory.CreateDocumentCollectionUri("ssp", "books"));
            return books.AsEnumerable();
        }
    }
}
