﻿using Lab2.EF;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab2.Cosmos
{
    public class CosmosUser : UserBase
    {
        [JsonProperty(propertyName: "id")]
        public string Id { get; set; }

        [JsonProperty(propertyName: "password")]
        public string PasswordHash { get; set; }

        [JsonProperty(propertyName: "salt")]
        public string PasswordSalt { get; set; }
    }
}
