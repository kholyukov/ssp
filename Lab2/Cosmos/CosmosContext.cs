﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab2.Cosmos
{
    public static class CosmosContext
    {
        const string cosmosUri = "https://xulios.documents.azure.com:443/";

        const string cosmosKey = "mrQukKAl6QSpYeuCdUxM6DujmT7UzNCEzWdEurzrxJbx6ukAOdXCf9kt5DIEkTTivTIIWOWgWuue2cGoKtBBCA==";

        const string databaseId = "ssp";

        const string bookCollectionId = "books";

        const string userCollectionId="users";

        static DocumentClient client = new DocumentClient(new Uri(cosmosUri), cosmosKey);

        static CosmosContext()
        {
            Initialize();

        }

        static private void Initialize()
        {
            client.CreateDatabaseIfNotExistsAsync(new Database()
            {
                Id = databaseId
            }).Wait();

            client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri(databaseId), new DocumentCollection()
            {
                Id = bookCollectionId
            });

            client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri(databaseId), new DocumentCollection()
            {
                Id = userCollectionId
            });
        }

        public static DocumentClient Client { get { return client; } }
    }
}
