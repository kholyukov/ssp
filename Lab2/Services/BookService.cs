﻿using Lab2.EF;
using Lab2.Models;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Lab2.Services
{
    public class BookService : IBookService
    {
        Context _context;
        IHostingEnvironment _environment;

        public BookService(Context context, IHostingEnvironment env)
        {
            _environment = env;
            _context = context;
        }

        public IEnumerable<Book> GetAll()
        {
            return _context.Books;
        }

        public void Create(CreateVM model)
        {
            Book book = new Book
            {
                Author = model.Author,
                Description = model.Description,
                Name = model.Name,
                Price = model.Price,
                ReleaseDate = model.ReleaseDate
            };
            if (model.Image != null)
            {
                string path = @"\images\" + $"{model.Image.FileName}-{model.Image.FileName}";
                string absolutePath = _environment.WebRootPath + path;
                using (var fileStream = new FileStream(absolutePath, FileMode.Create))
                {
                    model.Image.CopyTo(fileStream);
                }
                book.ImageLocation = path;
            }
            _context.Books.Add(book);
            _context.SaveChanges();
        }

        public void Create(Book book)
        {
            _context.Books.Add(book);
            _context.SaveChanges();
        }

        public Book Get(string id)
        {
            return _context.Books.Find(id);
        }

        public IEnumerable<Book> GetByName(string bookName)
        {
            var result = _context.Books.Where(b => b.Name.Contains(bookName, StringComparison.OrdinalIgnoreCase)).OrderBy(b => b.Price);
            return result;
        }

        public void Edit(EditVM model)
        {
            Book book = _context.Books.Find(model.Id);
            book.Name = model.Name;
            book.Price = model.Price;
            if (model.ReleaseDate != DateTime.MinValue)
            {
                book.ReleaseDate = model.ReleaseDate;
            }
            book.Author = model.Author;
            book.Description = model.Description;
            if (model.Image != null)
            {
                File.Delete(_environment.WebRootPath + book.ImageLocation);
                string path = @"\images\" + $"{model.Image.FileName}-{model.Image.FileName}";
                string absolutePath = _environment.WebRootPath + path;
                using (var fileStream = new FileStream(absolutePath, FileMode.Create))
                {
                    model.Image.CopyTo(fileStream);
                }
                book.ImageLocation = path;
            }
            _context.Entry(book).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(string id)
        {
            var book = _context.Books.Find(id);
            if (book.ImageLocation != null)
            {
                string absolutePath = _environment.WebRootPath + book.ImageLocation;
                File.Delete(absolutePath);
            }
            _context.Books.Remove(book);
            _context.SaveChanges();
        }
    }
}
