﻿using Lab2.EF;
using Lab2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab2.Services
{
    public interface IBookService
    {
        IEnumerable<Book> GetAll();
        void Create(CreateVM model);
        void Create(Book book);
        Book Get(string id);
        IEnumerable<Book> GetByName(string bookName);
        void Edit(EditVM model);
        void Delete(string id);
    }
}
