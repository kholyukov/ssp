﻿using Lab2.EF;
using Lab2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab2.Services
{
    public interface IUserService
    {
        Task<UserBase> FindOnLoginAsync(string email, string passwordString);
        Task CreateAsync(RegisterViewModel model);
        Task<bool> IsEmailExistAsync(string email);
    }
}
